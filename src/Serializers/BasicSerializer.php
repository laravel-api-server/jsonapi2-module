<?php namespace ApiServer\JsonApi2\Serializers;

use Illuminate\Contracts\Container\Container;
use DateTime;

use Tobscure\JsonApi\Collection;
use Tobscure\JsonApi\Relationship;
use Tobscure\JsonApi\Resource;
use Tobscure\JsonApi\SerializerInterface;
use Tobscure\JsonApi\AbstractSerializer;

class BasicSerializer extends AbstractSerializer {
    /**
     * @var Container
     */
    protected static $container;

    public function getRelationship($model, $name)
    {
        if ($relationship = $this->getCustomRelationship($model, $name)) {
            return $relationship;
        }

        return parent::getRelationship($model, $name);
    }

    public function getCustomRelationship($model, $relationName)
    {
        if (is_callable([$this, $relationName])) {
            $relationship = $this->$relationName($model);
            if ($relationship !== null && ! ($relationship instanceof Relationship)) {
                return false;
            }
            return $relationship;
        }
    }

    /**
     * @return Container
     */
    public static function getContainer()
    {
        return static::$container;
    }

    /**
     * @param Container $container
     */
    public static function setContainer(Container $container)
    {
        static::$container = $container;
    }

    public function getId($model)
    {
        return $model->getKey();
    }

    /**
     * @param DateTime|null $date
     * @return string|null
     */
    protected function formatDate(DateTime $date = null)
    {
        if ($date) {
            return $date->format(DateTime::RFC3339);
        }
    }

    /**
     * Get a relationship builder for a has-one relationship.
     *
     * @param mixed $model
     * @param string|Closure|\Tobscure\JsonApi\SerializerInterface $serializer
     * @param string|Closure|null $relation
     * @return Relationship
     */
    public function hasOne($model, $serializer, $relation = null)
    {
        return $this->buildRelationship($model, $serializer, $relation);
    }
    /**
     * Get a relationship builder for a has-many relationship.
     *
     * @param mixed $model
     * @param string|Closure|\Tobscure\JsonApi\SerializerInterface $serializer
     * @param string|null $relation
     * @return Relationship
     */
    public function hasMany($model, $serializer, $relation = null)
    {
        return $this->buildRelationship($model, $serializer, $relation, true);
    }
    /**
     * @param mixed $model
     * @param string|Closure|\Tobscure\JsonApi\SerializerInterface $serializer
     * @param string|null $relation
     * @param bool $many
     * @return Relationship
     */
    protected function buildRelationship($model, $serializer, $relation = null, $many = false)
    {
        if (is_null($relation)) {
            list(, , $caller) = debug_backtrace(false, 3);
            $relation = $caller['function'];
        }

        $data = $this->getRelationshipData($model, $relation);
        if ($data) {
            $serializer = $this->resolveSerializer($serializer, $model, $data);
            $type = $many ? Collection::class : Resource::class;
            $element = new $type($data, $serializer);
            return new Relationship($element);
        }
    }
    /**
     * @param mixed $model
     * @return mixed
     */
    protected function getRelationshipData($model, $relation)
    {
        if (is_object($model)) {
            return $model->$relation;
        } elseif (is_array($model)) {
            return $model[$relation];
        }
    }

    /**
     * @param mixed $serializer
     * @param mixed $model
     * @param mixed $data
     * @return SerializerInterface
     * @throws \InvalidArgumentException
     */
    protected function resolveSerializer($serializer, $model, $data)
    {
        if ($serializer instanceof Closure) {
            $serializer = call_user_func($serializer, $model, $data);
        }
        if (is_string($serializer)) {
            $serializer = $this->resolveSerializerClass($serializer);
        }
        if (! ($serializer instanceof SerializerInterface)) {
            throw new \InvalidArgumentException('Serializer must be an instance of '
                .SerializerInterface::class);
        }
        return $serializer;
    }

    /**
     * @param string $class
     * @return object
     */
    protected function resolveSerializerClass($class)
    {
        $serializer = static::$container->make($class);
        return $serializer;
    }
}

?>
