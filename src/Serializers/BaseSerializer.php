<?php

namespace ApiServer\JsonApi2\Serializers;

use ApiServer\JsonApi2\Serializers\BasicSerializer;


class BaseSerializer extends BasicSerializer {
    protected $type = null;
    protected $resource = null;
    protected $model = null;

    public function __call($method, $parameters) {
        try {
            $runtimeRelation = get_serializer_relation(static::class, $method);
        } catch(\BadMethodCallException $e) {
            // if runtime relation does not exist, then call method of parent
            // class
            return parent::__call($method, $parameters);
        }
        return $runtimeRelation($this, $parameters[0]);
	}

    public function getAttributes($model, array $fields = null)
    {
        // check model type
        if (! ($model instanceof $this->model)) {
            throw new \InvalidArgumentException(
                get_class($this).' can only serialize instances of '.$this->model
            );
        }

        // set up attributes
        $attributes = $model->getAttributes();
        $attributes['created_at']  = $this->formatDate($model->created_at);
        $attributes['updated_at'] = $this->formatDate($model->updated_at);
        return $attributes;
    }

    public function getLinks($model) {
        //links to always include in the resource
        $links = [
            'self' => config('app.url')."/{$this->resource}/{$model->id}",
        ];

        //links to include based permissions
        if(\Gate::allows('show', $model))
            $links['read'] = config('app.url')."/{$this->resource}/{$model->id}";
        if(\Gate::allows('update', $model))
            $links['update'] = config('app.url')."/{$this->resource}/{$model->id}";
        if(\Gate::allows('destroy', $model))
            $links['delete'] = config('app.url')."/{$this->resource}/{$model->id}";

        return $links;
    }
}

?>
