<?php

namespace ApiServer\JsonApi2\Services\EloquentCrudServices;

use ApiServer\JsonApi2\Services\EloquentProcessingServices\EloquentIncludeService;
use ApiServer\JsonApi2\Services\LinkService;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Database\Eloquent\Model;

use Tobscure\JsonApi\Document as JsonApiDocument;
use Tobscure\JsonApi\Resource as JsonApiResource;

use ApiServer\JsonApi2\Services\ResolveService;
use ApiServer\JsonApi2\Services\AbstractWriteService;

class EloquentCreateService extends AbstractWriteService
{
    private $model;

    public function __construct(Request $request)
    {
        $this->setRequest($request)
             ->setResolveService(app(ResolveService::class))
             ->setLinkService(new LinkService());
        $this->setIncludeService(new EloquentIncludeService());

        $this->parseRequest();
    }

    protected function getBaseSchemaName()
    {
        return 'schema';
    }

    protected function getWriteSchemaName()
    {
        return 'schema-create';
    }

    protected function processData() : Model
    {
        $this->setModel(app(ResolveService::class)->resolveType(
            $this->getType()
        ));

        $this->model->fill($this->getAttributes());
        if($this->getId() != null) {
            $this->model->{$this->getModel()->getKeyName()} = $this->getId();
        }
        $this->model->save();

        return $this->getModel();
    }

    public function buildResponse(int $statusCode = 201) : JsonResponse
    {
        return parent::buildResponse($statusCode);
    }
}