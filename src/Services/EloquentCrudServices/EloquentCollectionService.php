<?php

namespace ApiServer\JsonApi2\Services\EloquentCrudServices;

use ApiServer\JsonApi2\Services\EloquentProcessingServices\EloquentSortService;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;

use Tobscure\JsonApi\Collection as JsonApiCollection;
use Tobscure\JsonApi\Document as JsonApiDocument;

use ApiServer\JsonApi2\Services\LinkService;
use ApiServer\JsonApi2\Services\AbstractCollectionService;
use ApiServer\JsonApi2\Services\ResolveService;
use ApiServer\JsonApi2\Services\EloquentProcessingServices\EloquentFilterService;
use ApiServer\JsonApi2\Services\EloquentProcessingServices\EloquentIncludeService;
use ApiServer\JsonApi2\Services\EloquentProcessingServices\EloquentPaginationService;

class EloquentCollectionService extends AbstractCollectionService
{
    private $queryBuilder;

    public function __construct(
        Builder $queryBuilder,
        Request $request = null
    ) {
        $this->setQueryBuilder($queryBuilder);

        $this->setRequest($request)
              ->setResolveService(app(ResolveService::class))
              ->setLinkService(new LinkService());
        $this->setFilterService(new EloquentFilterService())
             ->setIncludeService(new EloquentIncludeService())
             ->setPaginationService(new EloquentPaginationService())
             ->setSortService(new EloquentSortService());

        $this->parseRequest();
    }

    public function getQueryBuilder() : Builder
    {
        return $this->queryBuilder;
    }

    public function setQueryBuilder(
        Builder $queryBuilder
    ) : EloquentCollectionService
    {
        $this->queryBuilder = $queryBuilder;
        return $this;
    }

    protected function parseRequest() : void
    {
        $this->getFilterService()->parse($this->getRequest());
        $this->getIncludeService()->parse($this->getRequest());
        $this->getSortService()->parse($this->getRequest());
        $this->getPaginationService()->parse($this->getRequest());
    }

    protected function processData() : LengthAwarePaginator
    {
        $this->getFilterService()->apply($this);
        $this->getIncludeService()->apply($this);
        $this->getSortService()->apply($this);
        $this->setPaginatedCollection(
            $this->getPaginationService()->apply($this)
        );
        return $this->getPaginatedCollection();
    }

    protected function buildDocument() : JsonApiDocument
    {
        if(is_null($this->getPaginatedCollection())) $this->processData();

        $rc = new \ReflectionClass($this->getQueryBuilder()->getModel());
        $className = $rc->getName();

        $serializerInstance = $this->getResolveService()->resolveModel(
            $className
        )->getSerializerInstance();

        $jsonApiCollection = new JsonApiCollection(
            $this->getPaginatedCollection()->items(),
            $serializerInstance
        );
        $jsonApiCollection->with($this->getIncludeService()->getIncludes());

        $this->setDocument(
            new JsonApiDocument($jsonApiCollection)
        );
        $this->getPaginationService()->addPaginationLinks(
            $this->getPaginatedCollection(),
            $this->getDocument()
        );

        return $this->getDocument();
    }
}