<?php

namespace ApiServer\JsonApi2\Services\EloquentCrudServices;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use Tobscure\JsonApi\Document as JsonApiDocument;

use ApiServer\JsonApi2\Services\AbstractResourceService;

class EloquentDeleteService extends AbstractResourceService
{
    private $queryBuilder;
    private $id;
    private $request;

    public function __construct(
        Builder $queryBuilder,
        $id = null,
        Request $request = null
    ) {
        $this->queryBuilder = $queryBuilder;

        $this->setId($id)
             ->setRequest($request);

        $this->parseRequest();
    }

    protected function parseRequest(): void
    {
        // TODO: Implement parseRequest() method.
    }

    protected function processData() : Model {
        $this->setModel(
            $this->queryBuilder->getModel()
        );

        if(!is_null($this->id)) {
            $this->queryBuilder = $this->queryBuilder
                ->where($this->getModel()->getKeyName(), $this->id);
        }
        $this->queryBuilder->delete();

        return null;
    }

    public function buildResponse(int $statusCode = 204): JsonResponse
    {
        parent::buildResponse($statusCode);
    }
}