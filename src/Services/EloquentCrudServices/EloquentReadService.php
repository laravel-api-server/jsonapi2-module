<?php

namespace ApiServer\JsonApi2\Services\EloquentCrudServices;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use Tobscure\JsonApi\Resource as JsonApiResource;
use Tobscure\JsonApi\Document as JsonApiDocument;

use ApiServer\JsonApi2\Services\ResolveService;
use ApiServer\JsonApi2\Services\LinkService;
use ApiServer\JsonApi2\Services\AbstractResourceService;
use ApiServer\JsonApi2\Services\EloquentProcessingServices\EloquentIncludeService;


class EloquentReadService extends AbstractResourceService
{
    private $queryBuilder;
    private $id;

    public function __construct(
        Builder $queryBuilder,
        $id = null,
        Request $request = null
    ) {
        $this->queryBuilder = $queryBuilder;

        $this->setId($id)
             ->setRequest($request)
             ->setResolveService(app(ResolveService::class))
             ->setLinkService(new LinkService());
        $this->setIncludeService(new EloquentIncludeService());

        $this->parseRequest();
    }

    protected function parseRequest() : void
    {
        $this->getIncludeService()->parse($this->getRequest());
    }

    protected function processData() : Model
    {
        $this->setModel($this->queryBuilder->getModel());
        if(!is_null($this->id)) {
            $this->queryBuilder = $this->queryBuilder
                ->where($this->getModel()->getKeyName(), $this->id);
        }
        $this->getIncludeService()->apply($this);

        return $this->queryBuilder->firstOrFail();
    }
}