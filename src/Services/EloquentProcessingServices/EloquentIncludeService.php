<?php

namespace ApiServer\JsonApi2\Services\EloquentProcessingServices;

use ApiServer\JsonApi2\Services\AbstractIncludeService;
use ApiServer\JsonApi2\Services\AbstractJsonApiService;
use Illuminate\Database\Eloquent\Builder;

class EloquentIncludeService extends AbstractIncludeService {
    public function apply(
        AbstractJsonApiService $jsonApiService
    ) : AbstractJsonApiService
    {
        $queryBuilder = $jsonApiService->getQueryBuilder();
        $queryBuilder->with($this->includes);
        $jsonApiService->setQueryBuilder($queryBuilder);
        return $jsonApiService;
    }
}