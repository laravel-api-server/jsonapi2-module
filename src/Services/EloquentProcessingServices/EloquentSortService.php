<?php

namespace ApiServer\JsonApi2\Services\EloquentProcessingServices;

use ApiServer\JsonApi2\Services\AbstractIncludeService;
use ApiServer\JsonApi2\Services\AbstractJsonApiService;
use ApiServer\JsonApi2\Services\AbstractSortService;
use Illuminate\Database\Eloquent\Builder;

class EloquentSortService extends AbstractSortService {
    public function apply(
        AbstractJsonApiService $jsonApiService
    ) : AbstractJsonApiService
    {
        $queryBuilder = $jsonApiService->getQueryBuilder();
        foreach($this->getSortings() as $sorting) {
            $queryBuilder->orderBy($sorting['column'], $sorting['order']);
        }
        $jsonApiService->setQueryBuilder($queryBuilder);
        return $jsonApiService;
    }
}