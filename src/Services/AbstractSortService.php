<?php

namespace ApiServer\JsonApi2\Services;

use Illuminate\Http\Request;

abstract class AbstractSortService {
    protected $sortings = [];

    public function parse(Request $request) : array
    {
        if($request->has('sort')) {
            $rawSortings = explode(",", $request->get('sort'));
            foreach($rawSortings as $rawSorting) {
                if(mb_substr($rawSorting, 0, 1) == '-') {
                    $this->sortings[] = [
                        'column' => mb_substr($rawSorting, 1, strlen($rawSorting)),
                        'order' => 'desc',
                    ];
                }
            }
        }

        return $this->sortings;
    }

    public abstract function apply(
        AbstractJsonApiService $jsonApiService
    ) : AbstractJsonApiService;

    public function getSortings() : array
    {
        return $this->sortings;
    }
}