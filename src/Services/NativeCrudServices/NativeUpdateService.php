<?php

namespace ApiServer\JsonApi2\Services\NativeCrudServices;

use ApiServer\JsonApi2\Services\LinkService;
use ApiServer\JsonApi2\Services\NativeProcessingServices\NativeIncludeService;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Database\Eloquent\Model;

use Tobscure\JsonApi\Document as JsonApiDocument;
use Tobscure\JsonApi\Resource as JsonApiResource;

use ApiServer\JsonApi2\Services\ResolveService;
use ApiServer\JsonApi2\Services\AbstractWriteService;

class NativeUpdateService extends AbstractWriteService {
    private $updateClosure;

    public function __construct(
        \Closure $updateClosure,
        Request $request
    )
    {
        $this->updateClosure = $updateClosure;

        $this->setRequest($request)
            ->setResolveService(app(ResolveService::class))
            ->setLinkService(new LinkService());
        $this->setIncludeService(new NativeIncludeService());

        $this->parseRequest();
    }

    protected function getBaseSchemaName()
    {
        return 'schema';
    }

    protected function getWriteSchemaName()
    {
        return 'schema-update';
    }

    protected function processData() : Model
    {
        $this->setModel(
            ($this->updateClosure)($this)
        );
        return $this->getModel();
    }
}