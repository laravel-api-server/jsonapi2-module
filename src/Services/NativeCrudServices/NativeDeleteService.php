<?php

namespace ApiServer\JsonApi2\Services\NativeCrudServices;

use ApiServer\JsonApi2\Services\AbstractResourceService;
use ApiServer\JsonApi2\Services\LinkService;
use ApiServer\JsonApi2\Services\NativeProcessingServices\NativeIncludeService;
use ApiServer\JsonApi2\Services\ResolveService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Tobscure\JsonApi\Document as JsonApiDocument;

class NativeDeleteService extends AbstractResourceService
{
    private $deletionClosure;

    public function __construct(
        \Closure $deletionClosure,
        $id,
        Request $request = null
    ) {
        $this->deletionClosure = $deletionClosure;
        $this->setId($id)
             ->setRequest($request)
             ->setResolveService(app(ResolveService::class))
             ->setLinkService(new LinkService());
        $this->setIncludeService(new NativeIncludeService());

        $this->parseRequest();
    }

    public function getId()
    {
        return $this->id;
    }

    protected function parseRequest(): void
    {
        // TODO: Implement parseRequest() method.
    }

    protected function processData() : ?Model
    {
        ($this->deletionClosure)($this);

        return null;
    }

    protected function buildDocument(): ?JsonApiDocument
    {
        $this->processData();

        return null;
    }

    public function buildResponse(int $statusCode = 204): JsonResponse
    {
        return parent::buildResponse($statusCode);
    }
}