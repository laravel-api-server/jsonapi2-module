<?php

namespace ApiServer\JsonApi2\Services\NativeCrudServices;

use ApiServer\JsonApi2\Services\LinkService;
use ApiServer\JsonApi2\Services\NativeProcessingServices\NativeIncludeService;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Database\Eloquent\Model;

use Tobscure\JsonApi\Document as JsonApiDocument;
use Tobscure\JsonApi\Resource as JsonApiResource;

use ApiServer\JsonApi2\Services\ResolveService;
use ApiServer\JsonApi2\Services\AbstractWriteService;

class NativeCreateService extends AbstractWriteService {
    private $createClosure;

    public function __construct(
        \Closure $createClosure,
        Request $request
    )
    {
        $this->createClosure = $createClosure;

        $this->setRequest($request)
            ->setResolveService(app(ResolveService::class))
            ->setLinkService(new LinkService());
        $this->setIncludeService(new NativeIncludeService());

        $this->parseRequest();
    }

    protected function getBaseSchemaName()
    {
        return 'schema';
    }

    protected function getWriteSchemaName()
    {
        return 'schema-create';
    }

    protected function processData() : Model
    {
        $this->setModel(
            ($this->createClosure)($this)
        );
        return $this->getModel();
    }

    public function buildResponse(int $statusCode = 201) : JsonResponse
    {
        return parent::buildResponse($statusCode);
    }
}