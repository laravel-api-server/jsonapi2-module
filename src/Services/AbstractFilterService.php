<?php

namespace ApiServer\JsonApi2\Services;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

abstract class AbstractFilterService {
    protected $filters = [];

    public function parse(Request $request) : void
    {
        if(!$request->has('filter'))
            return;

        $rawFilters = $request->get('filter');

        if(!is_array($rawFilters))
            throw new \Exception("Filter format is invalid.");

        foreach($rawFilters as $key=>$filter) {
            $this->filters[$key] = explode(",", $filter);
        }
    }

    public abstract function apply(
        AbstractCollectionService $collectionService
    ) : AbstractCollectionService;
}