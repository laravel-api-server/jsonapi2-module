<?php

namespace ApiServer\JsonApi2\Services\NativeProcessingServices;

use ApiServer\JsonApi2\Services\AbstractPaginationService;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Tobscure\JsonApi\Document;

class NativePaginationService extends AbstractPaginationService {
    public function apply(Collection $collection) : LengthAwarePaginator
    {
        return new LengthAwarePaginator(
            $collection,
            $collection->count(),
            $this->itemsPerPage,
            $this->currentPage
        );
    }
}