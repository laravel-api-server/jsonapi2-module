<?php

namespace ApiServer\JsonApi2\Services\NativeProcessingServices;

use ApiServer\JsonApi2\Services\AbstractCollectionService;
use ApiServer\JsonApi2\Services\AbstractFilterService;
use Illuminate\Http\Request;

class NativeFilterService extends AbstractFilterService {
    public function parse(Request $request): void
    {

    }

    public function apply(
        AbstractCollectionService &$collectionService
    ) : AbstractCollectionService
    {
        return $collectionService;
    }
}