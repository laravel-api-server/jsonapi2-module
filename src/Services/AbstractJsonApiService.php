<?php

namespace ApiServer\JsonApi2\Services;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

use Tobscure\JsonApi\Document as JsonApiDocument;

abstract class AbstractJsonApiService {
    protected $request;
    protected $document;
    protected $resolveService;
    protected $linkService;
    protected $asyncProcessing = false;

    protected abstract function parseRequest() : void;

    protected function setRequest(Request $request) : AbstractJsonApiService
    {
        $this->request = $request;
        return $this;
    }

    protected function getRequest() : Request
    {
        return $this->request;
    }

    public function getResolveService() : ResolveService
    {
        return $this->resolveService;
    }

    public function setResolveService(
        ResolveService $resolveService
    ) : AbstractJsonApiService
    {
        $this->resolveService = $resolveService;
        return $this;
    }

    public function setLinkService(
        LinkService $linkService
    ) : AbstractJsonApiService
    {
        $this->linkService = $linkService;
        return $this;
    }

    public function getLinkService() : LinkService
    {
        return $this->linkService;
    }

    protected function setDocument(JsonApiDocument $document) : AbstractJsonApiService
    {
        $this->document = $document;
        return $this;
    }

    protected function getDocument() : ?JsonApiDocument
    {
        return $this->document;
    }

    protected function asyncProcessingEnabled() : bool
    {
        return $this->asyncProcessing;
    }

    protected abstract function buildDocument() : ?JsonApiDocument;

    public function buildResponse(
        int $statusCode = 200
    ) : JsonResponse
    {
        if(is_null($this->getDocument())) $this->buildDocument();

        $statusCode = ($this->asyncProcessingEnabled())
            ? 202   // Accepted
            : $statusCode
        ;

        return response()
            ->json($this->getDocument())
            ->setStatusCode($statusCode);
    }
}