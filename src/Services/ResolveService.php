<?php

namespace ApiServer\JsonApi2\Services;

use ApiServer\JsonApi2\Exceptions\Exceptions\UnknownTypeException;
use ApiServer\JsonApi2\Models\ResolveBinding;
use Illuminate\Database\Eloquent\Model;
use Tobscure\JsonApi\SerializerInterface;

class ResolveService {
    private $bindings;

    public function __construct()
    {
        $this->bindings = collect([]);
    }

    public function addBinding(ResolveBinding $binding) {
        $this->bindings->push($binding);
    }

    public function resolveType($type) : ResolveBinding
    {
        $binding = $this->bindings->firstWhere('type', $type);
        if(is_null($binding)) {
            throw new UnknownTypeException("No binding set for type ".$type);
        }
        return $binding;
    }

    public function resolveModel(string $model) : ResolveBinding
    {
        foreach($this->bindings as $binding) {
            \Log::info($model);
            \Log::info($binding->model);

            if($binding->model == $model)
                return $binding;
        }

        throw new UnknownTypeException("No binding set for model ".$model);
    }
}