<?php

namespace ApiServer\JsonApi2\Exceptions\Handler;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Symfony\Component\HttpFoundation\Response;
use Tobscure\JsonApi\Exception\Handler\ResponseBag;
use ApiServer\ErrorHandler\Contracts\ExceptionHandler;

use Illuminate\Database\Eloquent\RelationNotFoundException;
use ApiServer\JsonApi2\Exceptions\Exceptions\BodyValidationException;
use ApiServer\JsonApi2\Exceptions\Exceptions\InvalidFilterException;

class BadRequestExceptionHandler extends ExceptionHandler {
    /**
     * {@inheritdoc}
     */
    protected function managesRoute(Route $route): bool {
        return(strpos($route->getName(), 'jsonapi.') === 0);
    }

    /**
     * {@inheritdoc}
     */
    protected function managesException(Exception $e): bool {
        return (    $e instanceof RelationNotFoundException
                    || $e instanceof BodyValidationException
                    || $e instanceof InvalidFilterException
        );
    }

    /**
     * {@inheritdoc}
     */
    public function handle(Exception $e, Request $request): Response {
        $errors = [];
        $status = 400;

        if($e instanceof RelationNotFoundException) {
            $errors[] = [
                    'detail' => $e->getMessage()
            ];
        }

        if( $e instanceof BodyValidationException
            || $e instanceof InvalidFilterException) {
            $messageBag = $e->getErrors();
            foreach($messageBag->getMessages() as $key=>$errorMessage) {
                $errors[] = [
                    'code' => "$key",
                    'detail' => $errorMessage
                ];
            }
        }

        return response()->jsonapiErrorResponse(
            $status,
            new ResponseBag($status, $errors)
        );
    }
}
