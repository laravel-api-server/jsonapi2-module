<?php

namespace ApiServer\JsonApi2\Exceptions\Exceptions;

class UnknownTypeException extends \Exception
{
    protected $code = 400;

    public function __construct (
        $message = "Unknown resource type.",
        \Exception $previous = NULL
    ) {
        parent::__construct($message, $this->code, $previous);
    }
}
