<?php

namespace ApiServer\JsonApi2\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Routing\ResponseFactory;
use Tobscure\JsonApi\Exception\Handler\ResponseBag;
class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @param  ResponseFactory  $factory
     * @return void
     */
    public function boot(ResponseFactory $factory)
    {
        $factory->macro('jsonapiErrorResponse', function (ResponseBag $responseBag) use ($factory) {
            $document = new \Tobscure\JsonApi\Document();
            $document->setErrors($responseBag->getErrors());
            return $factory->make($document, $responseBag->getStatus());
        });
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
