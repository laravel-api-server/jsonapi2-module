<?php

namespace ApiServer\JsonApi2\Providers;

use ApiServer\JsonApi2\Providers\ErrorHandlerServiceProvider;
use ApiServer\JsonApi2\Providers\ResponseMacroServiceProvider;
use ApiServer\JsonApi2\Services\ResolveService;
use ApiServer\JsonApi2\Services\SerializerRelationService;
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

use ApiServer\JsonApi2\Serializers\BasicSerializer;

class JsonApiServerProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Router $router) {
        BasicSerializer::setContainer($this->app);

        // boot cors middleware
        $router->aliasMiddleware(
            'cors',
            'Barryvdh\Cors\HandleCors'
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register('Barryvdh\Cors\ServiceProvider');

        //register all the service provider this user needs
        $this->app->register(ResponseMacroServiceProvider::class);
        $this->app->register(ErrorHandlerServiceProvider::class);

        // register the resolve service as singleton
        $this->app->singleton(ResolveService::class, function ($app) {
            return new ResolveService();
        });

        // register the serializer relation service as singleton
        $this->app->singleton(SerializerRelationService::class, function ($app) {
            return new SerializerRelationService();
        });
    }
}
