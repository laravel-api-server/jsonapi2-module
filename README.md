# Introduction to Laravel JsonApi-Server
This package is built for the Laravel framework and helps
you building a REST API following the
[json:api](http://jsonapi.org)-format.
It acts as a mapper between your API and the Eloquent
database layer (Eloquent services) but also provides support
for native data which is not necesarily bound to
the database (Native services).

[[_TOC_]]

## Features
The package provides methods for serving 
resources and resource-collections but
also provides methods for handling Create, Update
and Delete requests.

Each resource has support for:
 * Request header validation
 * Request body vaidation
 * Links (for [HATEOAS](https://en.wikipedia.org/wiki/HATEOAS))
 * Includes

In addition collections have support for: 
 * Pagination
 * Filtering

The package also supports a method for
binding JsonApi resource types to Eloquent-Models and 
JsonApi-Serializers.

The package currently does NOT have support for (contributions welcome!):
 * Creating/Updating/Deleting relations
 * Sparse fields
 * Sorting

## 3rd Party libraries
For assembling the data we use the [tobscure/json-api](https://github.com/tobscure/json-api) package.

## Terms
 * Serializer: Retrieves data and serializes it into a given format
 
# Basic Usage
## Registering the package
To register the package add
`ApiServer\JsonApi2\Providers\JsonApiServerProvider::class,`
to the `providers`-array of your `config/app.php`.

## Building Serializers
For building response in json:api-format we need
a serializer which take raw data and serializes it
into the wanted format.

We create a serializer class for each resource
type that we want to serve in our API. Therefore
we create a new directory `app/Serializers/` and
put our serializer classes inside.

The package already provides an abstract
BasicSerializer which you can use as a scaffold
to create your own serializers.


UserSerializer.php
```
class UserSerializer extends BaseSerializer
{
    protected $type = 'user';

    public function getAttributes($user, array $fields = null)
    {
        if (! ($user instanceof User)) {
            throw new \InvalidArgumentException(
                get_class($this).' can only serialize instances of '.User::class
            );
        }

        return parent::getAttributes($model, $fields);
    }

    public function getLinks($user) {
        $links = [
            'key' => 'value',
        ];

        return $links;
    }

    protected function role($user)
    {
        return $this->hasOne($user, RoleSerializer::class);
    }

    protected function permissions($user)
    {
        return $this->hasMany($user, PermissionSerializer::class);
    }
}
```

## Binding resource types
At many places the package acts as a mapper between
data sources (Native, Eloquent) and an output format.
To automate these mappings we need to bind
JsonApi-Types to Models and Serializers. This
is best done in a seperate service provider.

JsonApiResolveServiceProvider.php
```
class JsonApiResolveServiceProvider extends ServiceProvider
{
    public function register()
    {
        // nothing to do
    }

    public function boot()
    {
        $resolveService = app(ResolveService::class);
        $resolveService->addBinding(
            new ResolveBinding(
                'user',                // type
                UserSerializer::class,  // serializer
                User::class             // model (if type is bound to the database)
            )
        );
    }
}
```

## Serving a resource
To serve a resource we usually have a show method in
our controller:
```
public function show(Request $request, $id) {
    // code for serving resource
}
```

To serve an Eloquent Resource add
```
$jsonApiResourceService = new EloquentResourceService(
    User::query(),
    $id,
    $request
);

return $jsonApiResourceService->buildResponse();
```

If you want you can set the `$id`-parameter to `null` and add whatever
constraint you need to the query builder passed in the first parameter.
After building the query the service will call `firstOrFail` to
fetch the resource.

## Serving a collection of resources
To serve a collection we usually have an index method 
in our controller:
```
public function index(Request $request) {
    // code for serving collection
}
```

To serve a native Laravel collection 
```
$jsonApiCollectionService = new NativeCollectionService(
    collect([]),
    $request
);

return $jsonApiCollectionService->buildResponse();
```

To serve an Eloquent collection simply add:
```
$jsonApiCollectionService = new EloquentCollectionService(
    User::query(),
    $request
);

return $jsonApiCollectionService->buildResponse();
```

If you have special constraints to yor database query
beside the usual includes and filtering options
handled by the library you can simply
add them to the query instance
before passing it to the `EloquentCollectionService`.

## Creating resources
TODO

## Updating resorces
TODO

## Deleting resources
A method for deleting a resource in your controller usually looks
like this:
```
public function destroy(Request $request, $id) {
    // code for deleting resource
}
```

To delete a native resource we initialize the `NativeDeleteService`
with a callback which is called on deletion.
Inside the callback we can do whatever we want to
delete the resource:
```
$deleteService = NativeDeleteService(function() {
    $taskService = new TaskService();
    $taskService->removeTask($id);
});
return $deleteService->buildResponse();
```

To delete an Eloquent resource we just provide a
query builder as usual and an id to identify the
resource:
```
$deleteService = EloquentDeleteService(
    User::query(),
    $id
);
return $deleteService->buildResponse();
```

If you want you can set the `$id`-parameter to `null` and add whatever
constraint you need to the query builder passed in the first parameter.
After building the query the service will call `delete()` to
delete the resource.

# Digging deeper
## Filtering strategies



## Adding custom metadata

# Contributing
## Submitting patches
Patches can be submitted using the Merge-Request link of our gitlab.

## Mailinglist
https://lists.ffnw.de/mailman/listinfo/netmon-dev

# License
See [License](LICENSE.txt)
